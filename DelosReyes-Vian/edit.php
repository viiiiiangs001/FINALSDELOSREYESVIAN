<!DOCTYPE HTML>
<html>
    <head>
        <title>Online Beauty Shop</title>
        <link rel="stylesheet" type="text/css" href="css/edit.css">
    </head>
<body>
 
<h1>Update Existing Product</h1>
 
<?php

include 'db_connect.php';

if($_POST){

    $sql = "UPDATE
                products
            SET
                code = ?,
                name = ?,
                price = ?,
                quantity  = ?,
                manufacturer = ?
            WHERE
                id= ?";
 
    $stmt = $mysqli->prepare($sql);
 
    $stmt->bind_param(
        'sssssi',
        $_POST['code'],
        $_POST['name'],
        $_POST['price'],
        $_POST['quantity'],
        $_POST['manufacturer'],
        $_POST['id']
    );

    if($stmt->execute()){
        echo "<p> Success: Product Updated!! </p>";

        $stmt->close();
    }else{
        die("<p> Error: Unable to Update Product!! </p>");
    }
}
 
$sql = "SELECT
            id, code, name, price, quantity, manufacturer
        FROM
            products
        WHERE
            id = \"" . $mysqli->real_escape_string($_GET['id']) . "\"
        LIMIT
            0,1";

$result = $mysqli->query( $sql );

$row = $result->fetch_assoc();

extract($row);

$result->free();
$mysqli->close();
?>

<form action='edit.php?id=<?php echo $id; ?>' method='post' border='0'>
    <table>
        <tr>
            <td> Item Code: </td>
            <td><input type='text' name='code' value='<?php echo $code;  ?>' /></td>
        </tr>
        <tr>
            <td> Item Name: </td>
            <td><input type='text' name='name' value='<?php echo $name;  ?>' /></td>
        </tr>
        <tr>
            <td> Price: </td>
            <td><input type='text' name='price' value='<?php echo $price;  ?>' /></td>
        </tr>
        <tr>
            <td> Quantity: </td>
            <td><input type='text' name='quantity' value='<?php echo $quantity;  ?>' /></td>
        <tr>
        <tr>
            <td> Manufacturer: </td>
            <td><input type='text' name='manufacturer' value='<?php echo $manufacturer;  ?>' /></td>
        <tr>
            <td></td>
            <td>

                <input type='hidden' name='id' value='<?php echo $id ?>' />
                <input type='submit' value='Edit' class="btn-edit" />
                <a href='home.php'>Back to Home</a>
            </td>
        </tr>
    </table>
</form>
 
</body>
</html>