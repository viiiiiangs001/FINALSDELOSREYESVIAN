<!DOCTYPE HTML>
<html>
    <head>
        <title>Online Beauty Store</title>
        <link rel="stylesheet" type="text/css" href="css/register.css">
    </head>
<body>

<?php

if($_POST){

    include 'db_connection.php';

    $sql = "INSERT INTO
                accounts (firstname, lastname, username, password)
            VALUES
                (?, ?, ?, ?)";

    if($stmt = $mysqli->prepare($sql) ){

        $stmt->bind_param(
            "ssss",
			
            $_POST['firstname'],
            $_POST['lastname'],
            $_POST['username'],
            $_POST['password']
        );

        if($stmt->execute()){
            echo "<p> Success: Account Saved!! </p>";
            $stmt->close();
        }else{
            die("<p>Error: Cannot Create Account!!<p>");
        }
 
    }else{
        die("Error: Unable to Prepare Statement");
    }

    $mysqli->close();
}
 
?>
 
<h1>Online Beauty Store</h1>

	<div class="container">
		<img src="image/icon.png">
			<form action="register.php" method="post" border="0">
			<div class="form-input">
				<input type="text" name="firstname" placeholder="Input Firstname">
			</div>
			<div class="form-input">
				<input type="text" name="lastname" placeholder="Input Lastname">
			</div>
			<div class="form-input">
				<input type="text" name="username" placeholder="Input Username">
			</div>
			<div class="form-input">
				<input type="password" name="password" placeholder="Input Password">
			</div>
			<input type="submit" name="submit" value="CONFIRM" class="btn-confirm">
			<a href="login.php"> GO BACK </a>
		</form>
	</div>
</body>
</html>