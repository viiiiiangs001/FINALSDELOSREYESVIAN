<?php
include 'db_connect.php';

echo $_GET['id'];
$sql = "DELETE FROM products WHERE id = ?";

if($stmt = $mysqli->prepare($sql)){

    $stmt->bind_param("i", $_GET['id']);

    if($stmt->execute()){

        $stmt->close();

        header('Location: home.php?action=notif');
 
    }else{
        die("Error: Unable to Delete!!");
    }
 
}
?>