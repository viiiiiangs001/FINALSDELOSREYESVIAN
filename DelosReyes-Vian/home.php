<!DOCTYPE HTML>
<html>
    <head>
        <title>Online Beauty Store</title>
        <link rel="stylesheet" type="text/css" href="css/home.css">
    </head>
<body>
 
<h1>Online Beauty Store Product List</h1>
 
<?php

include 'db_connect.php';
 
$action = isset($_GET['action']) ? $_GET['action'] : "";

if($action=='deleted'){
}
 
$query = "select * from products";
$result = $mysqli->query( $query );
 
$num_results = $result->num_rows;

echo "<ul>";
    echo "<li><a href='home.php'> HOME </a></li>";
    echo "<li><a href='add.php'> NEW ITEM </a></li>";
    echo "<li><a href='index.php'> LOGOUT </a></li>";
echo "</ul>";
 
if( $num_results ){

    echo "<table border='1'>";

        echo "<tr>";
            echo "<th>Product Code</th>";
            echo "<th>Product Name</th>";
            echo "<th>Price</th>";
            echo "<th>Quantity</th>";
            echo "<th>Manufacturer</th>";
            echo "<th>Action</th>";
        echo "</tr>";

    while( $row = $result->fetch_assoc() ){

        extract($row);

        echo "<tr>";
            echo "<td>{$code}</td>";
            echo "<td>{$name}</td>";
            echo "<td>{$price}</td>";
            echo "<td>{$quantity}</td>";
            echo "<td>{$manufacturer}</td>";
            echo "<td>";
                echo "<a class='action' href='edit.php?id={$id}'>Edit</a>";
                echo " | ";
                echo "<a class='action' href='#' onclick='delete_item( {$id} );'>Delete</a>";
            echo "</td>";
        echo "</tr>";
    }

    echo "</table>";
 
}

else{
    echo "<p> No records found </p>";
}

$result->free();
$mysqli->close();
?>
 
<script type='text/javascript'>
function delete_item( id ){
 
    var answer = confirm('Are you sure?');

    if ( answer ){

        window.location = 'delete.php?id=' + id;
    }
}
</script>
 
</body>
</html>