<!DOCTYPE HTML>
<html>
    <head>
        <title>Online Beauty Shop</title>
        <link rel="stylesheet" type="text/css" href="css/add.css">
    </head>
<body>
 
<h1> Insert New Item </h1>
 
<?php

if($_POST){

    include 'db_connect.php';

    $sql = "INSERT INTO
                products (code, name, price, quantity, manufacturer)
            VALUES
                (?, ?, ?, ?, ?)";

    if($stmt = $mysqli->prepare($sql) ){

        $stmt->bind_param(
            "sssss",
			
            $_POST['code'],
            $_POST['name'],
            $_POST['price'],
            $_POST['quantity'],
            $_POST['manufacturer']
        );

        if($stmt->execute()){
            echo "<p> Success: Item Saved!! </p>";
            $stmt->close();
        }else{
            die("<p>Error: Cannot Save Item!!<p>");
        }
 
    }else{
        die("Error: Unable to Prepare Statement");
    }

    $mysqli->close();
}
 
?>

<div class="container">
	<img src="image/add.png">
	<form action='add.php' method='post' border='0'>
		<div class="form-input">
			<input type="text" name="code" placeholder="Input Product Code">
		</div>
		<div class="form-input">
			<input type="text" name="name" placeholder="Input Product Name">
		</div>
			<div class="form-input">
			<input type="text" name="price" placeholder="Input Price">
		</div>
		<div class="form-input">
			<input type="text" name="quantity" placeholder="Input Quantity">
		</div>
			<div class="form-input">
			<input type="text" name="manufacturer" placeholder="Input Manufacturer">
		</div>
			<input type="submit" name="submit" value="CONFIRM" class="btn-confirm">
			<a href="home.php"> GO BACK </a>
	</form>
</div>
 
</body>
</html>